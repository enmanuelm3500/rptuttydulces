package tutty.tuttyback.service;

import tutty.tuttyback.entity.Client;

import java.util.List;

public interface ClientService {

    List<Client> getList();

}
