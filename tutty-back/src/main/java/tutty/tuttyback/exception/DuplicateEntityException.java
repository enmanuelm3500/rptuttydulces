package tutty.tuttyback.exception;

import tutty.tuttyback.entity.AuditableEntity;

import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class DuplicateEntityException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.ENTITY.DUPLICATED";
    private static final String ERROR_MESSAGE_DEFAULT = "La entidad ya existe.";
    private static final String ERROR_MESSAGE_ONE = "El campo %s ya existe.";
    private static final String ERROR_MESSAGE_MANY = "Los campos %s ya existen.";
    private static final String DEVELOPER_MESSAGE = "An attempt was made to create a user that already exists";

    public DuplicateEntityException() {
        super(ERROR_CODE, ERROR_MESSAGE_DEFAULT, DEVELOPER_MESSAGE);
    }

    public DuplicateEntityException(String format, Object... args) {
        super(ERROR_CODE, format(format, args), DEVELOPER_MESSAGE);
    }

    public DuplicateEntityException(AuditableEntity entity) {
        this(getErrorMessage(entity));
    }

    public static String getErrorMessage(AuditableEntity entity) {
        Map<String, String> uniques = entity.getUniqueFields();
        String fields = uniques.entrySet()
                .stream()
                .map(entry -> entry.getValue().toLowerCase())
                .collect(Collectors.joining(", "));
        return uniques.size() > 1 ? format(ERROR_MESSAGE_MANY, fields) : format(ERROR_MESSAGE_ONE, fields);
    }

}
