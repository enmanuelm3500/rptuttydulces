package tutty.tuttyback.entity.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Clase que permite la conversión de valores {@link String[]} a {@link String} y viceversa para
 * las entidades del proyecto.
 *
 * @since 0.3.2
 */
@Converter
public class StringColumnConverter implements AttributeConverter<String[], String> {

    private static final String DEFAULT_SEPARATOR = ";";

    @Override
    public String convertToDatabaseColumn(String[] attribute) {
        return String.join(DEFAULT_SEPARATOR, attribute);
    }

    @Override
    public String[] convertToEntityAttribute(String dbData) {
        return dbData.split(DEFAULT_SEPARATOR);
    }
}
