package tutty.tuttyback.exception;

import static java.lang.String.format;

public class ApiKeyValidationException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.API.KEY.VALIDATION";
    private static final String ERROR_MESSAGE = "No es posible validar la ws key. Causa: %s.";
    private static final String DEVELOPER_MESSAGE = "API KEY INVALID.";

    public ApiKeyValidationException() {
        super(ERROR_CODE, ERROR_MESSAGE, DEVELOPER_MESSAGE);
    }

    public ApiKeyValidationException(String format, Object... args) {
        super(ERROR_CODE, format(format, args), DEVELOPER_MESSAGE);
    }

    public ApiKeyValidationException(String apiKey) {
        super(ERROR_CODE, format(ERROR_MESSAGE, apiKey), DEVELOPER_MESSAGE);
    }
}
