package tutty.tuttyback.exception;

public class WebRequestException extends Exception {

    private static final long serialVersionUID = 1955988877011555160L;

    public WebRequestException(String message) {
        super(message);
    }
}
