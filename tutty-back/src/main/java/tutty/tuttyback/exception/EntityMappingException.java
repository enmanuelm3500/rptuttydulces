package tutty.tuttyback.exception;

import tutty.tuttyback.entity.AuditableEntity;

import static java.lang.String.format;

public class EntityMappingException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.ENTITY.MAPPING";
    private static final String ERROR_MESSAGE = "%s no pudo ser procesado.";
    private static final String DEVELOPER_MESSAGE = null;

    public EntityMappingException() {
        super(ERROR_CODE, ERROR_MESSAGE, DEVELOPER_MESSAGE);
    }

    public EntityMappingException(String format, Object... args) {
        super(ERROR_CODE, format(format, args), DEVELOPER_MESSAGE);
    }

    public EntityMappingException(AuditableEntity entity) {
        this(format(ERROR_MESSAGE, entity));
    }
}
