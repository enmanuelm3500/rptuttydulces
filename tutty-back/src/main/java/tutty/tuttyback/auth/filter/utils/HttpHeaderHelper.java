package tutty.tuttyback.auth.filter.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.WebRequest;
import tutty.tuttyback.exception.WebRequestException;

import java.util.Optional;

/**
 * Clase de conveniencia que obtiene los headers de un request.
 */
public class HttpHeaderHelper {

    public static HttpHeaders getHttpHeaders(WebRequest request) throws WebRequestException {
        HttpHeaders httpHeaders = new HttpHeaders();
        Optional.ofNullable(request).orElseThrow(() -> new WebRequestException("WebRequest cannot be null"))
                .getHeaderNames()
                .forEachRemaining(
                        headerName ->
                                httpHeaders.set(headerName, request.getHeader(headerName)
                                ));
        return httpHeaders;
    }
}
