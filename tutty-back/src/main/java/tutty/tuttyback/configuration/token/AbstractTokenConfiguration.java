package tutty.tuttyback.configuration.token;

/**
 * Clase abstracta que contiene parámetros comunes de la configuración de los tokens.
 *
 * @since 0.4.0
 */
public class AbstractTokenConfiguration {

    private String secretKey;

    private String headerAuthorizationKey;

    private String bearerPrefix;

    private String issuerInfo;

    private long expiration;

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getHeaderAuthorizationKey() {
        return headerAuthorizationKey;
    }

    public void setHeaderAuthorizationKey(String headerAuthorizationKey) {
        this.headerAuthorizationKey = headerAuthorizationKey;
    }

    public String getBearerPrefix() {
        return bearerPrefix;
    }

    public void setBearerPrefix(String bearerPrefix) {
        this.bearerPrefix = bearerPrefix;
    }

    public String getIssuerInfo() {
        return issuerInfo;
    }

    public void setIssuerInfo(String issuerInfo) {
        this.issuerInfo = issuerInfo;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }
}
