package tutty.tuttyback.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tutty.tuttyback.entity.Users;

public interface UsersRepository extends JpaRepository <Users, Integer> {
    Users findByUsername(String userName);
}
