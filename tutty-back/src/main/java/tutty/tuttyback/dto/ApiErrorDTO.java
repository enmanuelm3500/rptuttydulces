package tutty.tuttyback.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;

public class ApiErrorDTO implements Serializable {

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private HttpStatus status;

    @JsonProperty("error_code")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String errorCode;

    @JsonProperty("cause")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String message;

    @JsonProperty("dev_message")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String devMessage;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd-HH:mm:ss")
    private LocalDateTime timestamp;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    private List<ValidationErrorDTO> details;

    public HttpStatus getStatus() {
        return status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public String getDevMessage() {
        return devMessage;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<ValidationErrorDTO> getDetails() {
        return details;
    }

    public ApiErrorDTO() {
        this.timestamp = LocalDateTime.now();
        this.details = new ArrayList<>();
    }

    public void addDetail(ValidationErrorDTO detail) {
        this.details.add(detail);
    }

    public static final class ApiErrorDTOBuilder {

        private static final String DEFAULT_MESSAGE = "Error inesperado";
        private static final String DEFAULT_DEV_MESSAGE = "Server Error";

        private HttpStatus status;
        private String errorCode;
        private String message;
        private String devMessage;
        private Throwable throwable;
        private List<ValidationErrorDTO> details;

        private ApiErrorDTOBuilder() {
            this.details = new ArrayList<>();
        }

        public static ApiErrorDTOBuilder create() {
            return new ApiErrorDTOBuilder();
        }

        public ApiErrorDTOBuilder withStatus(HttpStatus status) {
            this.status = status;
            return this;
        }

        public ApiErrorDTOBuilder withErrorCode(String errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public ApiErrorDTOBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ApiErrorDTOBuilder withDevMessage(String devMessage) {
            this.devMessage = devMessage;
            return this;
        }

        public ApiErrorDTOBuilder withDetail(List<ValidationErrorDTO> details) {
            this.details = details;
            return this;
        }

        public ApiErrorDTOBuilder withThrowable(Throwable throwable) {
            this.throwable = throwable;
            return this;
        }

        public ApiErrorDTO build() {
            ApiErrorDTO dto = new ApiErrorDTO();
            dto.status = ofNullable(this.status).isPresent() ? this.status : HttpStatus.INTERNAL_SERVER_ERROR;
            dto.errorCode = ofNullable(this.errorCode).isPresent() ? this.errorCode : "ERROR.INTERNAL";
            dto.message = ofNullable(this.message).isPresent() ? this.message : DEFAULT_MESSAGE;
            dto.devMessage = ofNullable(this.throwable).isPresent() ? this.throwable.getLocalizedMessage() : null;
            dto.devMessage = ofNullable(this.devMessage).isPresent() ? this.devMessage : DEFAULT_DEV_MESSAGE;
            this.details.forEach(dto::addDetail);
            return dto;
        }
    }
}
