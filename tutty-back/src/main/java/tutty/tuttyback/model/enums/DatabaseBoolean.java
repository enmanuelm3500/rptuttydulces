package tutty.tuttyback.model.enums;

public enum DatabaseBoolean {
    N(false, "N"),
    Y(true, "Y");

    private final Boolean value;
    private final String character;

    DatabaseBoolean(final boolean value, String character) {
        this.value = value;
        this.character = character;
    }

    public final Boolean getValue() {
        return value;
    }

    public String getCharacter() {
        return character;
    }
}
