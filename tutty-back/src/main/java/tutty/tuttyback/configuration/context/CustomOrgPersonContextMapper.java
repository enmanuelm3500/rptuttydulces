package tutty.tuttyback.configuration.context;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.util.Assert;

import java.util.Collection;

public class CustomOrgPersonContextMapper implements UserDetailsContextMapper {
    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
        CustomOrgPerson.Essence p = new CustomOrgPerson.Essence(ctx);

        p.setUsername(username);
        p.setAuthorities(authorities);

        return p.createUserDetails();
    }

    @Override
    public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
        Assert.isInstanceOf(CustomOrgPerson.class, user,
                "UserDetails must be an CustomOrgPerson instance");

        CustomOrgPerson p = (CustomOrgPerson) user;
        p.populateContext(ctx);
    }
}
