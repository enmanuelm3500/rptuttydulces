package tutty.tuttyback.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * Runtime exception for rest application
 */
public abstract class RestApplicationException extends RuntimeException {

    private final String errorCode;
    private final String message;
    private final String devMessage;
    private final Map<String, Object> details;

    public RestApplicationException(String errorCode, String message, String devMessage) {
        this.errorCode = errorCode;
        this.message = message;
        this.devMessage = devMessage;
        this.details = new HashMap<>();
    }

    public RestApplicationException(String errorCode, String message, String devMessage, Map<String, Object> details) {
        this(errorCode, message, devMessage);
        this.details.putAll(details);
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getDevMessage() {
        return devMessage;
    }

    public Map<String, Object> getDetails() {
        return details;
    }
}
