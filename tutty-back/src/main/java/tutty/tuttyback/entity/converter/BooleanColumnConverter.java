package tutty.tuttyback.entity.converter;

import tutty.tuttyback.model.enums.DatabaseBoolean;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static tutty.tuttyback.model.enums.DatabaseBoolean.*;
import static java.util.Optional.ofNullable;

/**
 * Clase que permite la conversión de valores {@link String} a {@link Boolean} y viceversa para
 * las entidades del proyecto.
 * @since 1.0
 */
@Converter
public class BooleanColumnConverter implements AttributeConverter<Boolean, String> {

    private DatabaseBoolean converted;

    /**
     * {@inheritDoc}
     */
    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        return convertToString(attribute);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        return convertToBoolean(dbData);
    }

    private String convertToString(Boolean attribute) {
        converted = Y;
        ofNullable(attribute)
                .ifPresent(
                value -> {
                    if(!value) {
                        converted = N;
                    }
                }
                );
        return converted.getCharacter();
    }

    private Boolean convertToBoolean(String dbData) {
        converted = Y;
        ofNullable(dbData)
                .ifPresent(
                        value -> converted = valueOf(value.toUpperCase())
                );
        return converted.getValue();
    }
}
