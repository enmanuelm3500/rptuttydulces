package tutty.tuttyback;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tutty.tuttyback.controller.ClientController;
import tutty.tuttyback.dto.ClientDto;
import tutty.tuttyback.repository.ClientRepository;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class TuttyBackApplicationTests {


	@InjectMocks
	private ClientController userController;

	@Mock
	private ClientRepository clientRepository;


}
