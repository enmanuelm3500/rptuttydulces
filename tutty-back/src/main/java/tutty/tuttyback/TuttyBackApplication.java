package tutty.tuttyback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import tutty.tuttyback.interceptor.LoggingInterceptor;

@SpringBootApplication
public class TuttyBackApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(TuttyBackApplication.class, args);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**");
    }

}
