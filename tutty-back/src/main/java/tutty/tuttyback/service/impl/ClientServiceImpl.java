package tutty.tuttyback.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tutty.tuttyback.entity.Client;
import tutty.tuttyback.repository.ClientRepository;
import tutty.tuttyback.service.ClientService;

import java.util.List;


@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Client> getList() {
        return (List<Client>) clientRepository.findAll();
    }

}
