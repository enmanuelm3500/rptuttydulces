package tutty.tuttyback.configuration.context;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.ldap.userdetails.Person;

public class CustomOrgPerson extends Person {

    private String carLicense;
    private String destinationIndicator;
    private String departmentNumber;
    private String displayName;
    private String employeeNumber;
    private String homePhone;
    private String homePostalAddress;
    private String initials;
    private String mail;
    private String mobile;
    private String o;
    private String ou;
    private String postalAddress;
    private String postalCode;
    private String roomNumber;
    private String street;
    private String title;
    private String uid;
    private String company;

    public String getUid() {
        return uid;
    }

    public String getMail() {
        return mail;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public String getInitials() {
        return initials;
    }

    public String getDestinationIndicator() {
        return destinationIndicator;
    }

    public String getO() {
        return o;
    }

    public String getOu() {
        return ou;
    }

    public String getTitle() {
        return title;
    }

    public String getCarLicense() {
        return carLicense;
    }

    public String getDepartmentNumber() {
        return departmentNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getHomePostalAddress() {
        return homePostalAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getStreet() {
        return street;
    }

    public String getCompany() {
        return company;
    }

    protected void populateContext(DirContextAdapter adapter) {
        super.populateContext(adapter);
        adapter.setAttributeValue("carLicense", carLicense);
        adapter.setAttributeValue("departmentNumber", departmentNumber);
        adapter.setAttributeValue("destinationIndicator", destinationIndicator);
        adapter.setAttributeValue("displayName", displayName);
        adapter.setAttributeValue("employeeNumber", employeeNumber);
        adapter.setAttributeValue("homePhone", homePhone);
        adapter.setAttributeValue("homePostalAddress", homePostalAddress);
        adapter.setAttributeValue("initials", initials);
        adapter.setAttributeValue("mail", mail);
        adapter.setAttributeValue("mobile", mobile);
        adapter.setAttributeValue("postalAddress", postalAddress);
        adapter.setAttributeValue("postalCode", postalCode);
        adapter.setAttributeValue("ou", ou);
        adapter.setAttributeValue("o", o);
        adapter.setAttributeValue("roomNumber", roomNumber);
        adapter.setAttributeValue("street", street);
        adapter.setAttributeValue("uid", uid);
        adapter.setAttributeValue("company", company);
        adapter.setAttributeValues("objectclass", new String[] { "top", "person",
                "organizationalPerson", "CustomOrgPerson" });
    }

    public static class Essence extends Person.Essence {
        public Essence() {
        }

        public Essence(CustomOrgPerson person) {
            super(person);
            setCarLicense(person.getCarLicense());
            setDepartmentNumber(person.getDepartmentNumber());
            setDestinationIndicator(person.getDestinationIndicator());
            setDisplayName(person.getDisplayName());
            setEmployeeNumber(person.getEmployeeNumber());
            setHomePhone(person.getHomePhone());
            setHomePostalAddress(person.getHomePostalAddress());
            setInitials(person.getInitials());
            setMail(person.getMail());
            setMobile(person.getMobile());
            setO(person.getO());
            setOu(person.getOu());
            setPostalAddress(person.getPostalAddress());
            setPostalCode(person.getPostalCode());
            setRoomNumber(person.getRoomNumber());
            setStreet(person.getStreet());
            setTitle(person.getTitle());
            setUid(person.getUid());
            setCompany(person.getCompany());
        }

        public Essence(DirContextOperations ctx) {
            super(ctx);
            setCarLicense(ctx.getStringAttribute("carLicense"));
            setDepartmentNumber(ctx.getStringAttribute("departmentNumber"));
            setDestinationIndicator(ctx.getStringAttribute("destinationIndicator"));
            setDisplayName(ctx.getStringAttribute("displayName"));
            setEmployeeNumber(ctx.getStringAttribute("employeeNumber"));
            setHomePhone(ctx.getStringAttribute("homePhone"));
            setHomePostalAddress(ctx.getStringAttribute("homePostalAddress"));
            setInitials(ctx.getStringAttribute("initials"));
            setMail(ctx.getStringAttribute("mail"));
            setMobile(ctx.getStringAttribute("mobile"));
            setO(ctx.getStringAttribute("o"));
            setOu(ctx.getStringAttribute("ou"));
            setPostalAddress(ctx.getStringAttribute("postalAddress"));
            setPostalCode(ctx.getStringAttribute("postalCode"));
            setRoomNumber(ctx.getStringAttribute("roomNumber"));
            setStreet(ctx.getStringAttribute("street"));
            setTitle(ctx.getStringAttribute("title"));
            setUid(ctx.getStringAttribute("uid"));
            setCompany(ctx.getStringAttribute("company"));
        }

        protected LdapUserDetailsImpl createTarget() {
            return new CustomOrgPerson();
        }

        public void setMail(String email) {
            ((CustomOrgPerson) instance).mail = email;
        }

        public void setUid(String uid) {
            ((CustomOrgPerson) instance).uid = uid;

            if (instance.getUsername() == null) {
                setUsername(uid);
            }
        }

        public void setInitials(String initials) {
            ((CustomOrgPerson) instance).initials = initials;
        }

        public void setO(String organization) {
            ((CustomOrgPerson) instance).o = organization;
        }

        public void setOu(String ou) {
            ((CustomOrgPerson) instance).ou = ou;
        }

        public void setRoomNumber(String no) {
            ((CustomOrgPerson) instance).roomNumber = no;
        }

        public void setTitle(String title) {
            ((CustomOrgPerson) instance).title = title;
        }

        public void setCarLicense(String carLicense) {
            ((CustomOrgPerson) instance).carLicense = carLicense;
        }

        public void setDepartmentNumber(String departmentNumber) {
            ((CustomOrgPerson) instance).departmentNumber = departmentNumber;
        }

        public void setDisplayName(String displayName) {
            ((CustomOrgPerson) instance).displayName = displayName;
        }

        public void setEmployeeNumber(String no) {
            ((CustomOrgPerson) instance).employeeNumber = no;
        }

        public void setDestinationIndicator(String destination) {
            ((CustomOrgPerson) instance).destinationIndicator = destination;
        }

        public void setHomePhone(String homePhone) {
            ((CustomOrgPerson) instance).homePhone = homePhone;
        }

        public void setStreet(String street) {
            ((CustomOrgPerson) instance).street = street;
        }

        public void setPostalCode(String postalCode) {
            ((CustomOrgPerson) instance).postalCode = postalCode;
        }

        public void setPostalAddress(String postalAddress) {
            ((CustomOrgPerson) instance).postalAddress = postalAddress;
        }

        public void setMobile(String mobile) {
            ((CustomOrgPerson) instance).mobile = mobile;
        }

        public void setHomePostalAddress(String homePostalAddress) {
            ((CustomOrgPerson) instance).homePostalAddress = homePostalAddress;
        }

        public void setCompany(String company) {
            ((CustomOrgPerson) instance).company = company;
        }
    }
}
