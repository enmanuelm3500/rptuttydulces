package tutty.tuttyback.configuration.token;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Clase que, instanciada al iniciar el servicio, toma los valores de las properties asociadas al manejo del
 * Token de autenticación.
 *
 * Esta clase se puese instanciar usado @Autowired.
 *
 * @since 0.2.6
 */
@Component(value = "tokenConfiguration")
@ConfigurationProperties(prefix="token")
public class TokenConfiguration extends AbstractTokenConfiguration {

}
