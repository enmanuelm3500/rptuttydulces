package tutty.tuttyback.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import tutty.tuttyback.entity.converter.BooleanColumnConverter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * Entidad auditable base para todas las entidades del proyecto
 */
@MappedSuperclass
public abstract class AuditableEntity {

    @Id
    @GenericGenerator(name = "idGenerator", strategy = "com.gire.rapientrega.entity.generator.EntityIdGenerator")
    @GeneratedValue(generator = "idGenerator")
    protected String id;

    @Column(name = "AUD_DATE_INSERT", nullable = false, updatable = false)
    @CreatedDate
    protected Timestamp createDate;
    @Column(name = "AUD_DATE_UPDATE")
    @LastModifiedDate
    protected Timestamp modifyDate;
    @Column(name = "AUD_USER_INSERT", nullable = false, updatable = false)
    @CreatedBy
    protected String createUser;
    @Column(name = "AUD_USER_UPDATE")
    @LastModifiedBy
    protected String modifyUser;
    @Convert(converter = BooleanColumnConverter.class)
    @Column(name = "IS_DELETED", nullable = false, columnDefinition = "boolean default false")
    protected boolean isDeleted;

    /**
     * @return un mapa con todos las constrains unique de la entidad: (nombre campo -> nombre columna)
     */
    public Map<String, String> getUniqueFields() {
        final Map<String, String> columns = new HashMap<>();
        Arrays.asList(this.getClass().getDeclaredFields()).forEach(field -> {
            Column column = field.getAnnotation(Column.class);
            if (column != null && column.unique()) {
                columns.put(field.getName(), column.name());
            }
        });
        return columns;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public Timestamp getModifyDate() {
        return modifyDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = (Optional.ofNullable(createDate).isPresent()) ? new Timestamp(createDate.getTime()) : null;
    }

    public void setModifyDate(Timestamp modifyDate) {
        this.modifyDate = modifyDate;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public void activate() {
        isDeleted = false;
    }

    public void delete() {
        isDeleted = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuditableEntity)) return false;
        AuditableEntity that = (AuditableEntity) o;
        return isDeleted == that.isDeleted &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isDeleted);
    }
}
