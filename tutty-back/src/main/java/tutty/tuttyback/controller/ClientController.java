package tutty.tuttyback.controller;


import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tutty.tuttyback.controller.mapper.ClientMapper;
import tutty.tuttyback.dto.ClientDto;
import tutty.tuttyback.entity.Users;
import tutty.tuttyback.repository.UsersRepository;
import tutty.tuttyback.service.ClientService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    private ClientMapper clientMapper;

    private ClientService clientService;

    @Autowired
    private UsersRepository usersRepository;

    public ClientController(ClientService clientService) {
        setClientService(clientService);
        setClientMapper(Mappers.getMapper(ClientMapper.class));
    }

    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<ClientDto> getList(Principal principal) {
        return clientMapper.toDtoList(clientService.getList());
    }

    public void setClientMapper(ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }



}
