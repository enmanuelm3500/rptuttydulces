package tutty.tuttyback.exception;

import org.springframework.security.access.AccessDeniedException;

import static java.lang.String.format;

public class ApiAccessDeniedException extends RestApplicationException {

    private static final String ERROR_CODE = "ERROR.ACCESS.DENIED";
    private static final String ERROR_MESSAGE = "No tiene los permisos suficientes para completar la solicitud.";
    private static final String DEVELOPER_MESSAGE = "Permission Error. %s";

    public ApiAccessDeniedException() {
        super(ERROR_CODE, ERROR_MESSAGE, DEVELOPER_MESSAGE);
    }

    public ApiAccessDeniedException(AccessDeniedException ex) {
        super(ERROR_CODE, ERROR_MESSAGE, format(DEVELOPER_MESSAGE, ex.getMessage()));
    }
}
