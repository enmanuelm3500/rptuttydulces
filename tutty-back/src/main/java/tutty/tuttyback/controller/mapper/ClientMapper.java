package tutty.tuttyback.controller.mapper;

import org.mapstruct.Mapper;
import tutty.tuttyback.dto.ClientDto;
import tutty.tuttyback.entity.Client;

import java.util.List;

@Mapper
public interface ClientMapper {

    ClientDto toDto(Client entity);

    Client toEntity(ClientDto dto);

    List<ClientDto> toDtoList(List<Client> clients);


}
