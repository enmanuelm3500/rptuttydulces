package tutty.tuttyback.auth.filter.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import tutty.tuttyback.exception.ApiKeyMissingException;
import tutty.tuttyback.exception.ApiKeyValidationException;
import tutty.tuttyback.exception.RestApplicationException;
import tutty.tuttyback.handler.RestResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

/**
 * Esta clase de conveniencia ayuda a manejar las excepciones que puedan surgir al momento de validar un token o durante
 * la generación del mismo.
 * <p>
 * Lo ideal sería usar {@link RestResponseEntityExceptionHandler}, pero, los filters funcionan por encima de la capa de
 * controllers, por tal motivo {@link RestResponseEntityExceptionHandler} no es capaz de manejar estas excepciones y
 * transformarlas en {@link ResponseEntity}. Esta clase hace el trabajo de {@link RestResponseEntityExceptionHandler},
 * pero invocado desde los distintos filters.
 *
 * @since 0.4.0
 */
public class WebServiceExceptionHandler {

    private final RestResponseEntityExceptionHandler restResponseEntityExceptionHandler;

    public WebServiceExceptionHandler() {
        restResponseEntityExceptionHandler = new RestResponseEntityExceptionHandler();
    }

    /**
     * Escribe en la response el mensaje provisto por la excepción. El código de estatus es provisto por
     * {@link RestResponseEntityExceptionHandler}
     *
     * @param ex       {@link RestApplicationException} excepción a procesar e incluir en la response
     * @param response elemento a transformar en respuesta para el clientes rest.
     */
    public void handleException(RestApplicationException ex, HttpServletResponse response) {
        try {
            if (ex instanceof ApiKeyMissingException) {
                handleException(ex, response, value -> restResponseEntityExceptionHandler
                        .handleBadRequest(ex));
            } else if (ex instanceof ApiKeyValidationException) {
                handleException(ex, response, value -> restResponseEntityExceptionHandler
                        .handleApiKeyValidationException((ApiKeyValidationException) ex));
            }
        } catch (Exception inner) {
            throw new AuthenticationServiceException("Error al intentar obtener el error de validación de ws", inner);
        }

    }

    private void handleException(RestApplicationException ex, HttpServletResponse response, Function<Exception, ResponseEntity> function) throws IOException {
        ResponseEntity entity = Optional
                .ofNullable(ex)
                .map(function)
                .orElseThrow(() -> new RuntimeException("Cannot handle exception"));
        response.setStatus(entity.getStatusCodeValue());
        response.setContentType(MediaType.APPLICATION_JSON_UTF8.toString());
        response.getWriter().write(convertObjectToJson(entity));
    }

    private String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
