package tutty.tuttyback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ValidationErrorDTO {

    @JsonProperty("object")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String object;

    @JsonProperty("field")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String field;

    @JsonProperty("rejected_value")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Object rejectedValue;

    @JsonProperty("message")
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String message;

    public ValidationErrorDTO(String object, String field, Object rejectedValue, String message) {
        this.object = object;
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.message = message;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getRejectedValue() {
        return rejectedValue;
    }

    public void setRejectedValue(Object rejectedValue) {
        this.rejectedValue = rejectedValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
