package tutty.tuttyback.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tutty.tuttyback.entity.Role;
import tutty.tuttyback.entity.Users;
import tutty.tuttyback.repository.UsersRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UsersDetailsAplication implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    /*@Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Users usuario = usersRepository.findByUsername(s);

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        *//*usuario.getRoles()
                .stream()
                .map(role -> {
                    grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority()));
                    return null;
                })
                .collect(Collectors.toList());*//*
        for(Role role : usuario.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        return new User(usuario.getUsername(),usuario.getPassword(),usuario.getEnabled(),true,true,true, grantedAuthorities);
    }*/

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users users = usersRepository.findByUsername(s);
        return buildUser(users);
    }

    private User buildUser(Users users){
            Set<GrantedAuthority> roles = buildAutority(users.getRoles());
        return new User(users.getUsername(),users.getPassword(),users.isEnabled(),true,true,true,roles);
    }


    private Set<GrantedAuthority> buildAutority(List<Role> roles){
        Set<GrantedAuthority> authorities = new HashSet<>();

        for(Role role: roles) {
            authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        if(authorities.isEmpty()) {
            throw new UsernameNotFoundException("Error en el Login: usuario  no tiene roles asignados!");
        }
        return authorities;
    }
}
